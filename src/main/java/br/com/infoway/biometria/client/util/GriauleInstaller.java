/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infoway.biometria.client.util;

/**
 *
 * @author humberto
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;

import com.griaule.grfingerjava.GrFingerJava;
import com.griaule.grfingerjava.GrFingerJavaException;

/**
 * This class is used to copy all native libraries used by Fingerprint SDK and
 * load them into the Java VM.
 */
public class GriauleInstaller {

    /**
     * Code for Microsoft Windows.
     */
    private static final int WINDOWS_OS = 0;
    /**
     * Code for Linux
     */
    private static final int LINUX_OS = 1;
    /**
     * Code for Unknown OS.
     */
    private static final int UNKNOWN_OS = 2;

    /**
     * Flag which indicates if Fingerprint SDK has already been installed.
     */
    private static boolean installed = false;

    /**
     * Copies Fingerprint SDK native libraries placed on the specified URL to a
     * local folder and make them available to thr JVM.
     *
     * @throws IOException
     */
    private static final Logger logger = Logger.getLogger(GriauleInstaller.class.getCanonicalName());

    public synchronized static void install(String productKey) throws IOException {

        //Skips re-installation.
        if (installed) {
            logger.info("Licença já instalada.");
            return;
        }

        logger.info("Iniciando processo de instalação com licença: ".concat(productKey));

        extractFiles();
        File destDir = getFinalDestDir();

        try {
            logger.info("Registrando biblioteca nativa.");
            GrFingerJava.setNativeLibrariesDirectory(destDir);
            logger.info("Registrando local da lincença.");
            GrFingerJava.setLicenseDirectory(destDir);

            if (!isLicensed(destDir)) {
                logger.info("Instalando a licença.");
                GrFingerJava.installLicense(productKey);
                logger.info("Licença instalada.");
            }
            installed = true;
        } catch (IllegalArgumentException e) {
            logger.log(Level.SEVERE, e.getMessage());
        } catch (GrFingerJavaException e) {
            logger.log(Level.SEVERE, e.getMessage());
        } catch (UnsatisfiedLinkError e) {
            logger.log(Level.SEVERE, e.getMessage());
        }

    }

    public static File getFinalDestDir() {
        String path = "C:" + System.getProperty("file.separator");

        if (getOS() == LINUX_OS) {
            path = System.getProperty("user.home") + System.getProperty("file.separator");
        }

        return new File(path + "BiometriaIhealth" + System.getProperty("file.separator") + "Griaule");

    }

    public static boolean extractFiles() {
        String path = "C:" + System.getProperty("file.separator");

        if (getOS() == LINUX_OS) {
            path = System.getProperty("user.home") + System.getProperty("file.separator");
        }

        String pathDestDirAnterior = path + "Iapep" + System.getProperty("file.separator") + "Griaule";

        File destDirAnterior = new File(pathDestDirAnterior);
        File destDir = getFinalDestDir();
        File licenseFileDest = extractFileLicense(destDir);

        if (licenseFileDest == null) {
            try {
                destDir.mkdirs();
                System.out.println("Moving..");
                File licenseFileAnterior = extractFileLicense(destDirAnterior);
                if (licenseFileAnterior != null) {
                    FileUtils.copyFileToDirectory(licenseFileAnterior, destDir);
                }
//                destDirAnterior.renameTo(destDir);
//                copyDirectory(destDirAnterior, destDir);
                addDir(destDir.getAbsolutePath());
            } catch (IOException e) {
                System.out.println("Not moved:" + destDirAnterior.getAbsolutePath());
                return false;
            }
        }

        try {
            String arch = arch();
            int filesSize = 0;
            if (arch != null && arch.equals("32")) {
                filesSize = 7;
            }

            if (arch != null && arch.equals("64")) {
                filesSize = 13;
            }

            String[] files = destDir.list();
            if (files != null && files.length < filesSize) {
                URL zipFile = GriauleInstaller.class.getClassLoader().getResource("sdk/" + arch + ".zip");

                ZipInputStream zipStream = new ZipInputStream(zipFile.openStream());

                logger.info("Copiando arquivos necessários para instalação.");
                copyFilesFor(destDir, zipStream);
                addDir(destDir.getAbsolutePath());
            }

        } catch (IOException e) {
            System.out.println("Not copied dlls files");
            return false;
        }

        System.out.println("Final Folder: " + destDir.getAbsolutePath());
        return true;
    }

    @Deprecated
    private static boolean foiMovidoParaDiretorioPadrao(File destDir) {
        boolean foiMovido = false;

        try {
            foiMovido = destDir.exists();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return foiMovido;
    }

    private static void copyDirectory(File sourceLocation, File targetLocation) throws IOException {
        if (sourceLocation.isDirectory()) {
            if (!targetLocation.exists()) {
                targetLocation.mkdir();
            }

            String[] children = sourceLocation.list();
            for (int i = 0; i < children.length; i++) {
                copyDirectory(new File(sourceLocation, children[i]), new File(targetLocation, children[i]));
            }
        } else {

            InputStream in = new FileInputStream(sourceLocation);
            OutputStream out = new FileOutputStream(targetLocation);

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }
    }

    /**
     * Finds out which OS / Plataform this JVM is being runned on.
     */
    private static int getOS() {
        //Loads the OS name / CPu architecture from system properties.
        String osName = System.getProperty("os.name");
        String arch = System.getProperty("os.arch");

        //Asserts this is a x86 compatible machine.
        //Fingerprint SDK is currently supported on this CPU architecture.
        if ((arch.equals("x86")) || (arch.equals("i386"))
                || (arch.equals("i486")) || (arch.equals("i586"))
                || (arch.equals("i686")) || (arch.equals("amd64"))) {

            //Selects one of the two different supported OS: Win/Linux
            if (osName.startsWith("Windows")) {
                return WINDOWS_OS;
            }
            if (osName.startsWith("Linux")) {
                return LINUX_OS;
            }
        }

        //OS has not been recognized (not supported)
        return UNKNOWN_OS;
    }

    public static void addDir(String s) throws IOException {
        try {
            // This enables the java.library.path to be modified at runtime
            // From a Sun engineer at http://forums.sun.com/thread.jspa?threadID=707176
            //
            Field field = ClassLoader.class.getDeclaredField("usr_paths");
            field.setAccessible(true);
            String[] paths = (String[]) field.get(null);
            for (int i = 0; i < paths.length; i++) {
                if (s.equals(paths[i])) {
                    return;
                }
            }
            String[] tmp = new String[paths.length + 1];
            System.arraycopy(paths, 0, tmp, 0, paths.length);
            tmp[paths.length] = s;
            field.set(null, tmp);
            System.setProperty("java.library.path", System.getProperty("java.library.path") + File.pathSeparator + s);
        } catch (IllegalAccessException e) {
            throw new IOException("Failed to get permissions to set library path");
        } catch (NoSuchFieldException e) {
            throw new IOException("Failed to get field handle to set library path");
        }
    }

    public static void copyFilesFor(File destDir, ZipInputStream zipStream) throws FileNotFoundException, IOException {
        ZipEntry zipEntry;
        nextFile:
        while ((zipEntry = zipStream.getNextEntry()) != null) {

            if ((getOS() == WINDOWS_OS && (zipEntry.getName().endsWith("dll") || zipEntry.getName().endsWith("bat")))
                    || (getOS() == LINUX_OS && (zipEntry.getName().endsWith("so")) || zipEntry.getName().endsWith("sh"))
                    || zipEntry.getName().endsWith("jar") || zipEntry.getName().endsWith("txt")) {

                for (String existFile : destDir.list()) {
                    if (existFile.equals(zipEntry.getName())) {
                        continue nextFile;
                    }
                }
                System.out.println("Copiando arquivo: " + zipEntry.getName());
                File f = new File(destDir, zipEntry.getName());
                if (zipEntry.isDirectory()) {
                    f.mkdirs();
                } else {
                    OutputStream out = new FileOutputStream(f);

                    byte[] buffer = new byte[4096];
                    while (true) {
                        int bytesRead = zipStream.read(buffer);
                        if (bytesRead == -1) {
                            break;
                        }
                        out.write(buffer, 0, bytesRead);
                    }
                    out.close();
                }
            }
        }
    }

    public static boolean isLicensed(File destDir) {
        return extractFileLicense(destDir) != null;
    }

    public static File extractFileLicense(File srcDir) {
        File fileLicense = null;
        if (srcDir != null) {
            File[] filesDir = srcDir.listFiles();
            if (filesDir != null && filesDir.length > 0) {
                for (File file : filesDir) {
                    if (file.getName().contains("GrFingerLicenseAgreement.txt")) {
                        fileLicense = file;
                        break;
                    }
                }
            }
        }

        return fileLicense;
    }

    public static String[] extractHardwareAndProductKey() throws FileNotFoundException, IOException {
    	extractFiles();
        File file = getFinalDestDir();
        File licenseFile = extractFileLicense(file);

        BufferedReader buffer = new BufferedReader(new FileReader(licenseFile));
        String hardwareKey = "";
        String licenseNumber = "";
        boolean hardwareKeyFounded = false;
        boolean licenceFounded = false;
        int lineNumber = 0;

        String linha = null;
        while ((linha = buffer.readLine()) != null) {
            if (linha == null || linha.isEmpty()) {
                continue;
            }

            if (linha.startsWith("HARDWARE") && !hardwareKeyFounded) {
                hardwareKeyFounded = true;
            }

            if (hardwareKeyFounded) {
                if (lineNumber == 3) {
                    hardwareKey = linha.trim();
                    hardwareKeyFounded = false;
                    lineNumber = 0;
                } else {
                    lineNumber++;
                }
            }

            if (linha.startsWith("LICENSE") && !licenceFounded) {
                licenceFounded = true;
            }

            if (licenceFounded) {
                if (lineNumber == 3) {
                    licenseNumber = linha.trim();
                    licenceFounded = false;
                    lineNumber = 0;
                } else {
                    lineNumber++;
                }
            }

        }
        buffer.close();
        return new String[]{hardwareKey, licenseNumber};
    }

    public static String arch() {

        String arch = System.getProperty("os.arch");
        if (arch.contains("86")) {
            return "32";
        } else if (arch.contains("64")) {
            return "64";
        }

        return null;
    }

}
