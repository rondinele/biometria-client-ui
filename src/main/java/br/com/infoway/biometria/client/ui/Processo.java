/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infoway.biometria.client.ui;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;

import br.com.infoway.biometria.client.util.GriauleInstaller;

/**
 *
 * @author humberto
 */
public class Processo {

	private final static String URL = "biometria.ihealth";

	private final static ClassLoader classLoader = Processo.class.getClassLoader();

	public static Passos bemVindo() {
		return Passos.BEM_VINDO;
	}

	public static void instalarLicenca(String licenca) throws Exception {
		String pathNameBase = GriauleInstaller.getFinalDestDir().getAbsolutePath();
		String command = "java -Dloader.path=\"" + pathNameBase + "\"  -jar  " + pathNameBase + "/app.jar " + licenca;

		File service = criarArquivo("install.bat", command);

		service.setExecutable(true);

		runtimeExecute(service.getAbsolutePath());
		FileUtils.forceDelete(service);
	}

	public static void instalarClient() throws Exception {
		GriauleInstaller.extractFiles();
		String pathNameBase = GriauleInstaller.getFinalDestDir().getAbsolutePath();
		File fileDest = new File(pathNameBase + "/app.jar");
		URL resource = classLoader.getResource("service/app.jar");

		FileUtils.copyURLToFile(resource, fileDest);
	}

	public static void instalarServico() throws Exception {
		criarServicoWindows();
		criarServicoLinux();
	}

	private static void criarServicoWindows() throws Exception {
		if (!System.getProperty("os.name").startsWith("Windows")) {
			return;
		}

		// addHostName("c:\\windows\\system32\\drivers\\etc\\hosts");
		String pathNameBase = GriauleInstaller.getFinalDestDir().getAbsolutePath();

		File service = criarArquivo(pathNameBase + "/service.bat",
				"java -Dloader.path=\"" + pathNameBase + "\"  -jar  " + pathNameBase + "/app.jar");

		service.setExecutable(true);

		String arch = GriauleInstaller.arch();
		URL nssm = classLoader.getResource("service/windows/win" + arch + "/nssm.exe");

		FileUtils.copyURLToFile(nssm, new File(pathNameBase.concat("/nssm.exe")));

		String nssmPath = pathNameBase + "/nssm.exe ";
		String servicePath = pathNameBase + "/service.bat ";

		runtimeExecute(nssmPath + "remove BiometriaIhealth confirm");
		runtimeExecute(nssmPath + "install BiometriaIhealth confirm");
		runtimeExecute(nssmPath + "set BiometriaIhealth DependOnService java");
		runtimeExecute(nssmPath + "set BiometriaIhealth Application " + servicePath);
		runtimeExecute(nssmPath + "set BiometriaIhealth AppDirectory " + pathNameBase);
		runtimeExecute(nssmPath + "set BiometriaIhealth DisplayName \"Biometria Ihealth\"");
		runtimeExecute(nssmPath + "set BiometriaIhealth Description \"Sistema de Biometria\"");
		runtimeExecute(nssmPath + "set BiometriaIhealth AppExit Default Restart");
		runtimeExecute(nssmPath + "set BiometriaIhealth Start SERVICE_AUTO_START");
		runtimeExecute(nssmPath + "start BiometriaIhealth");

	}

	private static void runtimeExecute(String command) throws IOException, InterruptedException {
		final Process exec = Runtime.getRuntime().exec(command);
		Thread.sleep(1000);
	}

	private static void addHostName(String pathHost) throws FileNotFoundException, IOException {
		File file = new File(pathHost);
		Scanner sc = new Scanner(file);

		String hostName = "127.0.0.1  biometria.infoway.com.br";
		while (sc.hasNextLine()) {
			if (sc.nextLine().contains(hostName)) {
				return;
			}
		}

		FileWriter fw = new FileWriter(file, true);
		fw.append(hostName);
		fw.flush();
		fw.close();
	}

	private static File criarArquivo(String caminho, String conteudo) {

		try {
			File file = new File(caminho);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(conteudo);
			bw.close();
			return file;
		} catch (IOException e) {
			return null;
		}

	}

	public static void finalizar() {

	}

	private static void criarServicoLinux() throws Exception {
		if (!System.getProperty("os.name").startsWith("Linux")) {
			return;
		}

		String pathNameBase = GriauleInstaller.getFinalDestDir().getAbsolutePath();
		File fileDest = new File(pathNameBase + "/service.sh");
		URL resource = classLoader.getResource("service/linux/daemon");

		FileUtils.copyURLToFile(resource, fileDest);
		String content = FileUtils.readFileToString(fileDest);
		content = content.replace("<service-description>", "Biometria Ihealth");
		content = content.replace("<service-name>", "BiometriaIhealth");
		content = content.replace("<path-bin-run>", "/usr/bin/java");
		content = content.replace("<arg-bin-run>",
				"-Dloader.path=" + pathNameBase + " -jar " + pathNameBase + "/app.jar");
		content = content.replace("<log-path>", pathNameBase);
		content = content.replace("<user-name>", System.getProperty("user.name"));
		content = content.replace("<user-group>", System.getProperty("user.name"));
		content = content.replace("<path-service>", pathNameBase);
		FileUtils.writeStringToFile(fileDest, content);

		runtimeExecute("chmod +x " + fileDest.getPath());
		runtimeExecute("sh " + fileDest.getPath() + " start");

		// addHostName("/etc/hosts");
	}

	public static boolean jaInstalado() {
		String pathNameBase = GriauleInstaller.getFinalDestDir().getAbsolutePath();
		File app = new File(pathNameBase + "/app.jar");

		return app.exists();
	}

	public static void desinstalar() {
		String pathNameBase = GriauleInstaller.getFinalDestDir().getAbsolutePath();
		try {
			if (System.getProperty("os.name").startsWith("Linux")) {
				String shPath = pathNameBase + "/service.sh";
				final File sh = new File(shPath);

				if (sh.exists()) {
					runtimeExecute(shPath + " stop");

					FileUtils.forceDelete(sh);
				}

			} else {
				String nssmPath = pathNameBase + "/nssm.exe";
				final File nssm = new File(nssmPath);

				if (nssm.exists()) {

					runtimeExecute(nssmPath + " stop BiometriaIhealth");
					runtimeExecute(nssmPath + " remove BiometriaIhealth confirm");

					FileUtils.forceDelete(nssm);
					final File service = new File(pathNameBase + "/service.bat");
					if (service.exists()) {
						FileUtils.forceDelete(service);
					}
				}

			}
			final File app = new File(pathNameBase + "/app.jar");
			if (app.exists()) {
				FileUtils.forceDelete(app);
			}

		} catch (IOException ex) {
			Logger.getLogger(Processo.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InterruptedException ex) {
			Logger.getLogger(Processo.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public static boolean licensaJaInstalada() {
		boolean isLicensed = false;
		String[] keys = null;
		try {
			keys = GriauleInstaller.extractHardwareAndProductKey();
			isLicensed = keys != null && keys.length > 1 && !keys[1].isEmpty();
		} catch (Exception ex) {
			Logger.getLogger(Processo.class.getName()).log(Level.SEVERE, "Linceça não instalada.");
			isLicensed = false;
		}

		return isLicensed;
	}

}
