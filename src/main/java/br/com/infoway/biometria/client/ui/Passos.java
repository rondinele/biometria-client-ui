/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infoway.biometria.client.ui;

/**
 *
 * @author humberto
 */
public enum Passos {

    BEM_VINDO("Instalar biometria do sistema iHealth?", true, true),
    JA_INSTALADO("Aplicativo já instalado.", false, true),
    JA_LICENCIADA("Máquina já licenciada", false, true),
    LICENCIAR("Insira a licença fornecida pelo iHealth.", true, true),
    INSTALANDO("Instalando...", false, false),
    FINALIZAR("Instalação concluída.", false, true), 
    DESINSTALANDO("Desinstalando ...", false, false), 
    DESINSTALADO("Desinstalado com sucesso.", false, true);

    private final String mensagem;

    private final boolean cancelar;
    private final boolean confirmar;

    private Passos(String mensagem, boolean cancelar, boolean confirmar) {
        this.mensagem = mensagem;
        this.cancelar = cancelar;
        this.confirmar = confirmar;
    }

    public String getMensagem() {
        return mensagem;
    }

    public boolean isCancelar() {
        return cancelar;
    }

    public boolean isConfirmar() {
        return confirmar;
    }

}
