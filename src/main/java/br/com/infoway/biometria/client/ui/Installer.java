/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infoway.biometria.client.ui;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author humberto
 */
public class Installer extends javax.swing.JFrame {

    /**
     * Creates new form Installer
     */
    private int passosInstalacao = 0;

    public Installer() {
        initComponents();
        mensagem.setText(Passos.BEM_VINDO.getMensagem());
        licensa.setVisible(false);
        aviso.setVisible(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mensagem = new javax.swing.JLabel();
        licensa = new javax.swing.JTextField();
        confirmar = new javax.swing.JButton();
        cancelar = new javax.swing.JButton();
        aviso = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        mensagem.setText("jLabel1");
        mensagem.setName(""); // NOI18N

        licensa.setText("AAAAA-AAAAA-AAAAA-AAAAA");
        licensa.setToolTipText("");
        licensa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                licensaActionPerformed(evt);
            }
        });

        confirmar.setText("Confirmar");
        confirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmarActionPerformed(evt);
            }
        });

        cancelar.setText("Cancelar");
        cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelarActionPerformed(evt);
            }
        });

        aviso.setText("jLabel1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(aviso, javax.swing.GroupLayout.PREFERRED_SIZE, 294, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(mensagem, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(licensa, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 294, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cancelar)
                        .addGap(140, 140, 140)
                        .addComponent(confirmar)))
                .addContainerGap(41, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addComponent(mensagem)
                .addGap(39, 39, 39)
                .addComponent(licensa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(aviso, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(confirmar)
                    .addComponent(cancelar))
                .addGap(43, 43, 43))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void licensaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_licensaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_licensaActionPerformed

    private void cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelarActionPerformed

    private void confirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmarActionPerformed
        status();

    }//GEN-LAST:event_confirmarActionPerformed

    private void status() {
        switch (passosInstalacao) {
            case 0:
                Processo.bemVindo();
                if (Processo.jaInstalado()) {
                    this.mensagem.setText(Passos.JA_INSTALADO.getMensagem());
                    this.licensa.setVisible(false);
                    this.cancelar.setVisible(true);
                    this.confirmar.setVisible(true);
                    this.confirmar.setText("Desinstalar");
                    passosInstalacao = 9;
                } else {
                    if (Processo.licensaJaInstalada()) {
                        this.mensagem.setText(Passos.JA_LICENCIADA.getMensagem());
                        this.licensa.setVisible(false);
                    } else {
                        this.mensagem.setText(Passos.LICENCIAR.getMensagem());
                        this.licensa.setVisible(true);
                    }
                    passosInstalacao = 1;
                }
                break;
            case 1:
                this.mensagem.setText(Passos.INSTALANDO.getMensagem());
                this.licensa.setVisible(false);
                this.cancelar.setVisible(true);
                this.confirmar.setVisible(false);
                final String key = this.licensa.getText();
                new Thread() {
                    @Override
                    public void run() {
                        try {
                            Processo.instalarClient();

                            if (!Processo.licensaJaInstalada()) {
                                Processo.instalarLicenca(key);
                            }
                            Processo.instalarServico();
                            passosInstalacao = 2;
                            status();
                        } catch (UnsatisfiedLinkError e) {
                            Logger.getLogger(Installer.class.getName()).log(Level.SEVERE, null, e);
                        } catch (Exception ex) {
                            Logger.getLogger(Installer.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }.start();
                break;
            case 2:
                this.mensagem.setText(Passos.FINALIZAR.getMensagem());
                this.cancelar.setVisible(false);
                this.confirmar.setVisible(true);
                passosInstalacao = 3;
                break;
            case 3:
                Processo.finalizar();
                this.dispose();
                passosInstalacao = 999;
                break;
            case 8:
                this.mensagem.setText(Passos.DESINSTALADO.getMensagem());
                this.confirmar.setText("Sair");
                this.confirmar.setVisible(true);
                this.cancelar.setVisible(false);
                passosInstalacao = 3;
                break;
            case 9:
                this.mensagem.setText(Passos.DESINSTALANDO.getMensagem());
                this.confirmar.setVisible(false);
                new Thread() {
                    @Override
                    public void run() {
                        Processo.desinstalar();
                        passosInstalacao = 8;
                        status();
                    }
                }.start();
                break;
            default:
                break;
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void inciar(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Installer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(Installer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Installer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            Logger.getLogger(Installer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Installer().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel aviso;
    private javax.swing.JButton cancelar;
    private javax.swing.JButton confirmar;
    private javax.swing.JTextField licensa;
    private javax.swing.JLabel mensagem;
    // End of variables declaration//GEN-END:variables
}
